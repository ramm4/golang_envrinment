package	main

import	(	"fmt"
			"bufio"
			"os"
			"strconv"
)

func checkError(err error){

	if err != nil {
		panic(err)
	}
}

func handshake(n int32) int32 {
	return (((n - 1) * n) / 2)
}

func main() {

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter test case: ")
	text, _, _ := reader.ReadLine()
	valTemp, err := strconv.ParseInt(string(text), 10, 64)
	checkError(err)

	t := int(valTemp)

	for tItr := 0; tItr < t; tItr++ {
		text, _, _ = reader.ReadLine()
		nTemp, err := strconv.ParseInt(string(text), 10, 64)
		checkError(err);
		result := handshake(int32(nTemp))

		fmt.Printf("%d\n", result)
	}
}
