package main

import ("fmt"
		"bufio"
		"os"
		"strconv")

func	check_err(err error, quantity_cases int64) {

		Max_Int32	  := 1<< 32 -1
		
		if err != nil {
		   panic(err)
		}
		if quantity_cases < 0 || quantity_cases > int64(Max_Int32) {
		   panic("Incorrect quantity cases")
		}
}

func	main(){

		var						nTmp int64
		reader					:= bufio.NewReader(os.Stdin)
		line,_ ,_				:= reader.ReadLine()
		quantity_cases, err		:= strconv.ParseInt(string(line), 10, 64)

		check_err(err, quantity_cases);

		for tItr := 0; tItr < int(quantity_cases); tItr++ {
			line, _, _ =  reader.ReadLine()
			nTmp, err = strconv.ParseInt(string(line), 10, 64)
			check_err(err, nTmp);
			fmt.Println(nTmp);
	 }
		fmt.Println(quantity_cases);
}