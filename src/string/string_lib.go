package string

func Reverse(s string) string {

	runes := []rune(s)
	middle_str_len := (len(runes) / 2)

	for i := 0; i < middle_str_len; i++ {
		j := len(runes) - i - 1
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string (runes)
}