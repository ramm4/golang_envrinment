package string

import (	"testing"	)

func Test(t *testing.T)	{

	tests := []struct {
		in, want string
	}{
		{"Hello, world", "dlrow ,olleH"},
		{"Hello, 世界", "界世 ,olleH"},
		{"", ""},
	}

	for _, c := range tests {
		got := Reverse(c.in)
		if got != c.want {
			t.Errorf("Reverse (%q) == %q, want %q", c.in, got, c.want)
		}
	}
}