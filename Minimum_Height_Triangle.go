package main
import (    "fmt"
            "bufio"
            "os"
            "strings"
            "strconv"
)

func main() {

    var mod int64 = 0
    reader := bufio.NewReader(os.Stdin)
    text, _, err := reader.ReadLine()

    if err != nil {
        panic (err)     
    }
    sp := strings.Split(string(text), " ")
    if len(sp) != 2 {
        panic("Error: data input didn't correct")
    } else {
                b, err_b := strconv.ParseInt(sp[0], 10, 64)
                area, err_area := strconv.ParseInt(sp[1], 10, 64)
                if err_b != nil || err_area != nil {
                    panic("Error: data input didn't correct")
                }
        if (area % b) > 0 {
            mod = 1;
        }
        h := (2 * (area)) / b + mod
        fmt.Println(int(h))
            }
}